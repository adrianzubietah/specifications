module.exports = {
    entry: ['babel-polyfill', 'whatwg-fetch', 'promise-polyfill', './src/main/js/App.jsx'],
    output: {
        path: __dirname,
        filename: './target/classes/static/built/bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    },
    performance: {
        hints: false
    }
}
