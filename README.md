# Example Specification
Full-stack app Java React

##Pre-requisites

Java 8
Node 8 y npm
Maven 3
MySQL 5.6


##How to run
##### In the project root:
    npm install
    
    mvn clean install
    
    java -jar target/base.jar
##### If you want to use hot-reloading for the js files, run

    npm run watch