package com.adrianzubieta.specification.controller.rest;

import com.adrianzubieta.specification.domain.Book;
import com.adrianzubieta.specification.service.BookService;
import com.adrianzubieta.specification.vo.FiltersVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/books")
public class BookRestController {

  private final BookService bookService;

  @GetMapping
  public Page<Book> findAll(Pageable pageable, FiltersVo filtersVo) {
    log.debug("filtersVo: [{}]", filtersVo.toString());
    Page<Book> books = bookService.findAll(filtersVo, pageable);
    log.debug("Total Elements: {}", books.getTotalElements());
    log.debug("Books encontrados: {}", books.getContent());
    return books;
  }

}
