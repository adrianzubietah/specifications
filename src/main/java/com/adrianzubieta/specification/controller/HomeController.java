package com.adrianzubieta.specification.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

  @RequestMapping({"/home"})
  String index() {
    return "index";
  }

}
