package com.adrianzubieta.specification.utils;

import com.adrianzubieta.specification.domain.Book;
import com.adrianzubieta.specification.vo.FiltersVo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class BookSpecification implements Specification<Book> {

  private static final char CHARACTER_LIKE = '%';
  private static final String AUTHOR_NAME = "authorName";
  private final FiltersVo filtersVo;

  @Override
  public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

    List<Predicate> predicates = new ArrayList<>();

    if (filtersVo.getKey().equals(AUTHOR_NAME)) {
      Predicate predicate =  criteriaBuilder
          .like(root.join("author").get("name"), CHARACTER_LIKE + filtersVo.getValue() + CHARACTER_LIKE);

      predicates.add(predicate);
    } else {
      Predicate predicate =  criteriaBuilder
          .like(root.get(filtersVo.getKey()), CHARACTER_LIKE + filtersVo.getValue() + CHARACTER_LIKE);

      predicates.add(predicate);
    }

    if (filtersVo.getPublished() != null) {
      Predicate predicate =  criteriaBuilder.equal(root.get("published"), filtersVo.getPublished());
      predicates.add(predicate);
    }

    criteriaQuery.distinct(true);
    return predicates.stream().reduce(criteriaBuilder::and).get();
  }


}
