package com.adrianzubieta.specification.vo;

import lombok.Data;

@Data
public class FiltersVo {

  private String key = "name";
  private String value = "";
  private Boolean published;

}
