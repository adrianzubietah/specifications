package com.adrianzubieta.specification.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String editorial;
  private String category;
  private Integer pageQuantity;
  private Boolean published;
  @ManyToOne
  private Author author;

}
