package com.adrianzubieta.specification.service.impl;

import com.adrianzubieta.specification.domain.Book;
import com.adrianzubieta.specification.repository.BookRepository;
import com.adrianzubieta.specification.service.BookService;
import com.adrianzubieta.specification.utils.BookSpecification;
import com.adrianzubieta.specification.vo.FiltersVo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

  private final BookRepository bookRepository;

  @Override
  public Page<Book> findAll(FiltersVo filtersVo, Pageable pageable) {
    BookSpecification bookSpecification = new BookSpecification(filtersVo);
    return bookRepository.findAll(bookSpecification, pageable);
  }
}
