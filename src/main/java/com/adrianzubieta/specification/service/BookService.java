package com.adrianzubieta.specification.service;

import com.adrianzubieta.specification.domain.Book;
import com.adrianzubieta.specification.vo.FiltersVo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {

  Page<Book> findAll(FiltersVo filtersVo, Pageable pageable);

}
