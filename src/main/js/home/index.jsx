import React from 'react';
import {
Typography,
    Table,
    Pagination,
    Input,
    Select,
    Icon,
} from 'antd';
import axios from 'axios';
import injectSheet from 'react-jss';

const { Column } = Table;
const { Title } = Typography;
const { Option } = Select;
const { Search } = Input;

const styles = {
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingRight: '3%',
        paddingLeft: '4%',
        background: 'black',
    },
    tittle: {
        marginTop: '2%',
        color: 'white !important',
    },
    pagination: {
        display: 'flex',
        margin: '2%',
        justifyContent: 'flex-end',
    },
    table: {
        '& tr:nth-child(2n+1)': {
            background: '#EDECEA',
        },
    },
    selectPublished: {
        marginLeft: '1%',
        width: '9%',
    },
    filter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '1%',
    }
  };

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filters: {
                published: '',
                filterKey: 'name',
                filterValue: '',
            },
            pageOptions: {
                totalElement: 0,
                pageNum: 1,
                pageSize: 10,
                sortField: 'name',
                sortValue: 'desc',
            },
            data: [],
            loading: true,
        };
    }

    async componentDidMount() {
        this.getData();
    }

    getData = async () => {
        const {
            pageOptions,
            filters,
        } = this.state;

        const {
            published,
            filterKey,
            filterValue,
        } = filters;

        const {
            pageNum,
            pageSize,
            sortField,
            sortValue,
        } = pageOptions;

        const url = `/api/books?page=${pageNum - 1}&size=${pageSize}&sort=${sortField},${sortValue}&published=${published}&key=${filterKey}&value=${filterValue}`;

        const response = await axios.get(url);
        this.setState((prevState) => ({
            pageOptions: {
                ...prevState.pageOptions,
                totalElement: response.data.totalElements,
            },
            data: response.data.content,
            loading: false,
        }));
    }

    handleTableChange = (pagination, filters, { order, field }) => {
        field === 'dropShipping' ? field = 'dropShipping.fileName' : '';
        this.setState((prevState) => ({
            pageOptions: {
                ...prevState.pageOptions,
                sortField: field,
                sortValue: order === 'ascend' ? 'asc' : 'desc',
            },
            loading: true,
        }), this.getData);
    }

    handlePagination = (pageNum) => {
        this.setState((prevState) => ({
            pageOptions: {
                ...prevState.pageOptions,
                pageNum,
            },
            loading: true,
        }), this.getData);
    };

    handleChangeFilterKey = (key) => {
        this.setState((prevState) => ({
            filters: {
                ...prevState.filters,
                filterKey: key,
            },
            loading: true,
        }), this.getData);
    }

    handleChangeFilterValue = (value) => {
        this.setState((prevState) => ({
            filters: {
                ...prevState.filters,
                filterValue: value,
            },
            loading: true,
        }), this.getData);
    }

    handleChangeSelectPublished = (value) => {
        this.setState((prevState) => ({
            filters: {
                ...prevState.filters,
                published: value,
            },
            loading: true,
        }), this.getData);
    }

    render() {
        const {
            classes,
        } = this.props;
        const {
            data,
            pageOptions,
            filters,
            loading,
        } = this.state;

        return (
            <div>
                <div className={classes.header}>
                    <Title className={classes.tittle}>Specifications</Title>
                </div>
                <div className={classes.filter}>
                    <Select
                        style={{ width: '13%' }}
                        defaultValue={filters.filterKey}
                        onChange={this.handleChangeFilterKey}
                    >
                        <Option value='name'>Nombre Del Libro</Option>
                        <Option value='authorName'>Nombre del Author</Option>
                        <Option value='category'>Categoria</Option>
                        <Option value='editorial'>Editorial</Option>
                    </Select>
                    <Search minLength={2} style={{ width: '25%' }} onSearch={this.handleChangeFilterValue} loading={loading} />
                    <Select
                        className={classes.selectPublished}
                        defaultValue="Todos"
                        onChange={this.handleChangeSelectPublished}
                    >
                        <Option value=''>Todos</Option>
                        <Option value='true'>Publicados</Option>
                        <Option value='false'>No Publicados</Option>
                    </Select>
                </div>
                <Table
                    className={classes.table}
                    dataSource={data}
                    rowKey={({ id }) => id}
                    pagination={false}
                    onChange={this.handleTableChange}
                    loading={loading}
                >
                    <Column
                        title="Id Book"
                        dataIndex="id"
                        key="id"
                        ellipsis
                        sorter
                        align="center"
                    />
                    <Column
                        title="Nombre"
                        dataIndex="name"
                        key="name"
                        sorter
                        align="center"
                    />
                    <Column
                        title="Editorial"
                        dataIndex="editorial"
                        key="editorial"
                        sorter
                        align="center"
                    />
                    <Column
                        title="Categoria"
                        dataIndex="category"
                        key="category"
                        sorter
                        align="center"
                    />
                    <Column
                        title="Cantidad de Paginas"
                        dataIndex="pageQuantity"
                        key="pageQuantity"
                        ellipsis
                        sorter
                        align="center"
                    />
                    <Column
                        title="Author"
                        dataIndex="author"
                        key="author"
                        ellipsis
                        align="center"
                        render = {({name}) => (
                            <div>
                                {name}
                            </div>
                        )}
                    />
                    <Column
                        title="Publicado"
                        dataIndex="published"
                        key="published"
                        ellipsis
                        align="center"
                        render = {(published) => (
                            <Icon
                                type={published ? 'check-circle' : 'close-circle'}
                                style={{ color: published ? 'green': 'red'  }}
                            />
                        )}
                    />
                </Table>
                <Pagination
                    className={classes.pagination}
                    current={pageOptions.pageNum}
                    pageSize={pageOptions.pageSize}
                    total={pageOptions.totalElement}
                    showTotal={(total, range) => `${range[0]}-${range[1]} de ${total} resultados`}
                    onChange={this.handlePagination}
                />
            </div>
        );
    }
}

export default injectSheet(styles)(Home);