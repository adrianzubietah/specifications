package com.adrianzubieta.specification.service;

import com.adrianzubieta.specification.SpecificationApplicationTests;
import com.adrianzubieta.specification.builders.AuthorBuilder;
import com.adrianzubieta.specification.builders.BookBuilder;
import com.adrianzubieta.specification.builders.FiltersVoBuilder;
import com.adrianzubieta.specification.domain.Author;
import com.adrianzubieta.specification.domain.Book;
import com.adrianzubieta.specification.vo.FiltersVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import static org.assertj.core.api.Assertions.assertThat;

public class BookServiceTest extends SpecificationApplicationTests {

  @Autowired
  private BookService bookService;

  @Test
  public void findAll_withoutFilters_returnThreeBooks() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf().build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(3L);
  }

  @Test
  public void findAll_withFilterName_returnBooksWithNameSearched() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf()
        .withKey("name")
        .withValue("secreta")
        .build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(1L);
    assertThat(books.getContent().get(0).getName()).isEqualTo("Harry Potter y la cámara secreta");
  }

  @Test
  public void findAll_withFilterCategory_returnBooksWithCategorySearched() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf()
        .withKey("category")
        .withValue("Aventura")
        .build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(1L);
    assertThat(books.getContent().get(0).getName()).isEqualTo("Moby Dick");
  }

  @Test
  public void findAll_withFilterEditorial_returnBooksWithEditorialSearched() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf()
        .withKey("editorial")
        .withValue("Bentley")
        .build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(1L);
    assertThat(books.getContent().get(0).getName()).isEqualTo("Moby Dick");
  }

  @Test
  public void findAll_withFilterAuthorName_returnBookWithAuthorNameSearched() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf()
        .withKey("authorName")
        .withValue("Melville")
        .build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(1L);
    assertThat(books.getContent().get(0).getName()).isEqualTo("Moby Dick");
  }

  @Test
  public void findAll_withFilterPublishedTrue_returnTwoBook() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf()
        .withPublished(true)
        .build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(2L);
  }

  @Test
  public void findAll_withFilterPublishedFalse_returnOneBook() {
    generateBooksInDataBase();

    FiltersVo filtersVo = FiltersVoBuilder
        .instanceOf()
        .withPublished(true)
        .build();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Book> books = bookService.findAll(filtersVo, pageable);

    assertThat(books.getContent().size()).isEqualTo(2L);
  }

  private void generateBooksInDataBase(){
    Author authorOne = AuthorBuilder
        .instanceOf()
        .withName("Herman Melville")
        .buildWithEntityManager(entityManager);

    BookBuilder.instanceOf()
        .withName("Moby Dick")
        .withCategory("Aventura")
        .withEditorial("Richard Bentley")
        .withPageQuantity(832)
        .withPublished(false)
        .withAuthor(authorOne)
        .buildWithEntityManager(entityManager);

    Author authorTwo = AuthorBuilder
        .instanceOf()
        .withName("J. K. Rowling")
        .buildWithEntityManager(entityManager);

    BookBuilder.instanceOf()
        .withName("Harry Potter y la piedra filosofal")
        .withCategory("fantasia")
        .withEditorial("editorial")
        .withPageQuantity(309)
        .withPublished(true)
        .withAuthor(authorTwo)
        .buildWithEntityManager(entityManager);

    BookBuilder.instanceOf()
        .withName("Harry Potter y la cámara secreta")
        .withCategory("fantasia")
        .withEditorial("editorial")
        .withPageQuantity(386)
        .withPublished(true)
        .withAuthor(authorTwo)
        .buildWithEntityManager(entityManager);
  }

}
