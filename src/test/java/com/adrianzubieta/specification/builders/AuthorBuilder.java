package com.adrianzubieta.specification.builders;

import com.adrianzubieta.specification.domain.Author;

import javax.persistence.EntityManager;

public class AuthorBuilder {

  private Author author;

  private AuthorBuilder() {
    this.author = new Author();
  }

  public static AuthorBuilder instanceOf() {
    return new AuthorBuilder();
  }

  public AuthorBuilder withId(Long id) {
    author.setId(id);
    return this;
  }

  public AuthorBuilder withName(String name) {
    author.setName(name);
    return this;
  }

  public Author build() {
    return author;
  }

  public Author buildWithEntityManager(EntityManager entityManager) {
    entityManager.persist(author);
    return author;
  }
}
