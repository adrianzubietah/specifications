package com.adrianzubieta.specification.builders;

import com.adrianzubieta.specification.domain.Author;
import com.adrianzubieta.specification.domain.Book;

import javax.persistence.EntityManager;

public class BookBuilder {

  private Book book;

  private BookBuilder() {
    this.book = new Book();
  }

  public static BookBuilder instanceOf() {
    return new BookBuilder();
  }

  public BookBuilder withId(Long id) {
    book.setId(id);
    return this;
  }

  public BookBuilder withCategory(String category) {
    book.setCategory(category);
    return this;
  }

  public BookBuilder withEditorial(String eqditorial) {
    book.setEditorial(eqditorial);
    return this;
  }

  public BookBuilder withPageQuantity(Integer pageQuantity) {
    book.setPageQuantity(pageQuantity);
    return this;
  }

  public BookBuilder withPublished(Boolean published) {
    book.setPublished(published);
    return this;
  }

  public BookBuilder withAuthor(Author author) {
    book.setAuthor(author);
    return this;
  }

  public BookBuilder withName(String name) {
    book.setName(name);
    return this;
  }

  public Book build() {
    return book;
  }

  public Book buildWithEntityManager(EntityManager entityManager) {
    entityManager.persist(book);
    return book;
  }
}
