package com.adrianzubieta.specification.builders;

import com.adrianzubieta.specification.vo.FiltersVo;

public class FiltersVoBuilder {

  private FiltersVo filtersVo;

  private FiltersVoBuilder() {
    this.filtersVo = new FiltersVo();
  }

  public static FiltersVoBuilder instanceOf() {
    return new FiltersVoBuilder();
  }


  public FiltersVoBuilder withKey(String key) {
    filtersVo.setKey(key);
    return this;
  }

  public FiltersVoBuilder withValue(String value) {
    filtersVo.setValue(value);
    return this;
  }

  public FiltersVoBuilder withPublished(boolean published) {
    filtersVo.setPublished(published);
    return this;
  }

  public FiltersVo build() {
    return filtersVo;
  }

}
